FROM localhost:5000/alpine-node-rd-kafka:8.11.3
RUN mkdir -p /app
COPY / /app
WORKDIR /app
RUN npm install && npm run build && cp env.js /app/build/src/env.js
WORKDIR /app/build/src
ENTRYPOINT [ "node", "index.js" ]
