/* tslint:disable */
import { v4 as uuid } from 'uuid';

const nodeId = uuid();
export const STORAGE_TYPES = {
  S3: 'aws',
  MINIO: 'minio',
};
let config = {
  domain: 'tradex',
  defaultFetchCount: 100,
  db: {
    client: 'mysql',
    connection: {
      host:
        'ls-7cb4c06adc779212e6cb3028d74965c3c413c80e.cq3lbezwn0da.ap-southeast-1.rds.amazonaws.com',
      port: 3306,
      user: 'techxdev',
      password: '7,B,2^H+mTK.:(O)po3iKPI0N$.7D<ox',
      database: 'tradex-configuration',
    },
  },
  logger: {
    config: {
      appenders: {
        application: { type: 'console' },
        file: { type: 'file', filename: '/logs/application.log', compression: true, maxLogSize: 10485760, backups: 10},
      },
      categories: {
        default: { appenders: ['application', 'file'], level: 'info' },
      },
    },
  },
  log: {
    serviceName: 'configuration-service',
    format: 'FLAT', // 'FLAT' or 'JSON'
    transport: [],
  },
  clusterId: 'configuration',
  clientId: `configuration-${nodeId}`,
  nodeId: nodeId,
  json: 'dbExport.json',
  kafkaUrls: ['localhost:9092'],
  zkUrls: ['localhost:2181'],
  storageService: STORAGE_TYPES.S3, // STORAGE_TYPES.MINIO
  topic: {
    configurationSync: 'configuration.sync',
    domainConnector: 'domain-connector',
  },
  uri: {
    findAllHoliday: '/api/v1/holidays',
    findAllInterestInfo: '/api/v1/interestInfo',
    getAllResourcesForInternal: '/api/v1/locale/internal',
    getAllResources: '/api/v1/locale',
    getAllAdminResources: '/api/v1/admin/locale/resource',
    getAllKeysByNamespace: '/api/v1/admin/locale/{namespaceId}/key',
  },
  aws: {
    accessKeyId: 'AKIAIOLXKEAYDTDCI2HA',
    secretAccessKey: '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',
    s3: {
      announcement: {
        region: 'ap-southeast-1',
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'announcement/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'text/html',
      },
      analysisReport: {
        region: 'ap-southeast-1',
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'analysis_report/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'application/pdf',
      },
      langResource: {
        region: 'ap-southeast-1',
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'lang_resource/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'application/json',
      },
      public: {
        region: 'ap-southeast-1',
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'avatar/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'image/',
      },
      dbExport: {
        region: 'ap-southeast-1',
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'json_file/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'application/json/',
      },
    },
  },
  minio: {
    externalEndpoint: 'https://tradex.vn/files',
    internalEndpoint: 'http://prod3.dev.tradex.vn:9000',
    port: 9000,
    accessKeyId: 'AKIAIOLXKEAYDTDCI2HA',
    secretAccessKey: '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',
    urlRewriteTo: 'http://localhost:9000',
    region: 'ap-southeast-1',
    policies: {
      'public-read': `{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["*"]},"Action":["s3:GetBucketLocation"],"Resource":["arn:aws:s3:::xxBucketNamexx"]},{"Effect":"Allow","Principal":{"AWS":["*"]},"Action":["s3:GetObject"],"Resource":["arn:aws:s3:::xxBucketNamexx/*"]}]}`,
    },
    buckets: {
      announcement: {
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'announcement/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'text/html',
      },
      analysisReport: {
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'analysis_report/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'application/pdf',
      },
      langResource: {
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'lang_resource/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'application/json',
      },
      public: {
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'avatar/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'image/',
      },
      dbExport: {
        acl: 'public-read',
        bucket: 'tradex-vn',
        expires: 300, //seconds
        pathToUpload: 'json_file/',
        minUpload: 0,
        maxUpload: 2097152, // 2 MiB;
        contentType: 'application/json/',
      },
    },
  },
  publicScopeGroup: 4,
  dbExportUrl:
    'https://tradex-vn.s3.ap-southeast-1.amazonaws.com/dbExport.json',
  assumeRole: {
    public: {
      DurationSeconds: 900,
      RoleArn: 'arn:aws:iam::157907901550:role/TradeX_Customer',
    },
  },
  kafkaCommonOptions: {},
  kafkaConsumerOptions: {},
  kafkaProducerOptions: {},
  kafkaTopicOptions: {},
  swagger: {
    server: 'localhost:3000',
    version: {
      v1: '/api/v1',
      v2: '/api/v2',
    },
    header: {
      openapi: '3.0.0',
      info: {
        title: 'Rest API Specification',
        version: '1.0.0',
        description: '![API Flow](/assets/img/API_Flow.png)',
      },
      servers: [
        {
          url: `localhost:3000`,
          description: 'TRADEX API Server',
        },
      ],
      components: {
        securitySchemes: {
          jwt: {
            type: 'apiKey',
            in: 'header',
            name: 'Authorization',
          },
        },
      },
    },
  },
  s3: {
    region: 'ap-southeast-1',
    bucketName: 'tradex-vn',
    accessKey: 'AKIAIOLXKEAYDTDCI2HA',
    privateKey: 'Dl6k9qgN7IQ4THu7m3G/AO/VwtK4YyqOJak0cyzu',
  },
};

try {
  const env = require('./env');
  console.log(env);
  if (env) {
    config = { ...config, ...env(config) };
  }
} catch (e) {
  //swalow it
}

config.kafkaConsumerOptions = {
  ...(config.kafkaCommonOptions ? config.kafkaCommonOptions : {}),
  ...(config.kafkaConsumerOptions ? config.kafkaConsumerOptions : {}),
};
config.kafkaProducerOptions = {
  ...(config.kafkaCommonOptions ? config.kafkaCommonOptions : {}),
  ...(config.kafkaProducerOptions ? config.kafkaProducerOptions : {}),
};

export default config;
