import { Service } from 'typedi';
import { AWS, Errors } from 'tradex-common';
import config, { STORAGE_TYPES } from '../config';
import IAWSGetSignedDataRequest from '../models/request/IAWSGetSignedDataRequest';
import { AWS_GET_SIGNED_DATA_FAILED } from '../constants/errors';
import { Client as MinioClient } from 'minio';

@Service()
export default class AmazonWebService {
  public async getSignedDataToUploadPublic(
    request: IAWSGetSignedDataRequest
  ): Promise<any> {
    if (config.storageService === STORAGE_TYPES.S3) {
      const signedData: any = await AWS.generateSignedDataForUpload(
        request.key,
        config.aws.s3.public
      );
      if (signedData == null) {
        throw new Errors.GeneralError(AWS_GET_SIGNED_DATA_FAILED, null);
      }
      return signedData;
    } else {
      const client = new MinioClient({
        endPoint: config.minio.externalEndpoint,
        accessKey: config.minio.accessKeyId,
        secretKey: config.minio.secretAccessKey,
        useSSL: config.minio.externalEndpoint.startsWith('https'),
        region: config.minio.region,
      });
      const conf = config.minio.buckets.public;
      if (!(await client.bucketExists(conf.bucket))) {
        await client.makeBucket(conf.bucket, config.minio.region);
        let policies = config.minio.policies[conf.acl];
        policies = policies.split('xxBucketNamexx').join(conf.bucket);
        await client.setBucketPolicy(
          conf.bucket,
          policies
        );
      }
      return client.presignedPutObject(conf.bucket, request.key, conf.expires);
    }
  }
}
