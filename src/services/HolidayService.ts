import { Service } from 'typedi';
import { TradexModelsConfiguration } from 'tradex-models-ts';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { HolidayRepository } from '../repositories/HolidayRepository';
import Holiday, { parseToHolidayResponse } from '../models/db/Holiday';

@Service()
export default class HolidayService {
  @InjectRepository()
  private readonly holidayRepository: HolidayRepository;

  public async findAllHoliday(
    request: TradexModelsConfiguration.HolidayListRequest
  ): Promise<TradexModelsConfiguration.HolidayListResponse> {
    const holidays: Holiday[] = await this.holidayRepository.find();
    return holidays.map(parseToHolidayResponse);
  }
}
