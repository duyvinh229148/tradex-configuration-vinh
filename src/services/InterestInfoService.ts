import { Service } from 'typedi';
import { TradexModelsConfiguration } from 'tradex-models-ts';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { InterestInfoRepository } from '../repositories/InterestInfoRepository';
import InterestInfo, { parseInterestInfos } from '../models/db/InterestInfo';

@Service()
export default class InterestInfoService {
  @InjectRepository()
  private readonly interestInfoRepository: InterestInfoRepository;

  public async findAllInterestInfo(): Promise<
    TradexModelsConfiguration.QueryInterestInfoResponse
  > {
    const interestInfoList: InterestInfo[] = await this.interestInfoRepository.find();
    return parseInterestInfos(interestInfoList);
  }
}
