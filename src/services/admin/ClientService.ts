import { Service } from 'typedi';
import { TradexModelsConfiguration } from 'tradex-models-ts';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { ClientRepository } from '../../repositories/ClientRepository';
import { Errors, Kafka, Utils } from 'tradex-common';
import * as validationUtils from '../../utils/validationUtils';
import Client, { parseToClientResponse } from '../../models/db/Client';
import LoginMethod from '../../models/db/LoginMethod';
import { LoginMethodRepository } from '../../repositories/LoginMethodRepository';
import { ClientStatusEnum } from '../../constants/ClientStatusEnum';
import { v4 as uuid } from 'uuid';
import {
  CLIENT_SYNC_TOPIC,
  CLIENT_UPDATE_TOPIC,
} from '../../constants/updateTopics';
import config from '../../config';

@Service()
export default class ClientService {
  @InjectRepository()
  private readonly clientRepository: ClientRepository;

  @InjectRepository()
  private readonly loginMethodRepository: LoginMethodRepository;

  public async findAllClient(
    request: TradexModelsConfiguration.QueryClientRequest
  ): Promise<TradexModelsConfiguration.QueryClientResponse> {
    const invalidParams = new Errors.InvalidParameterError();
    validationUtils.validateDomain(request.domain, invalidParams);
    validationUtils.validateFetchCount(request.fetchCount, invalidParams);
    validationUtils.validateLastSequence(request.lastSequence, invalidParams);
    validationUtils.validateIsFullData(request.isFullData, invalidParams);
    invalidParams.throwErr();

    const clientList: Client[] = await this.clientRepository.findByClientRequest(
      request
    );
    return clientList.map(parseToClientResponse);
  }

  public async queryClientForUpdate(
    request: TradexModelsConfiguration.QuerySystemClientRequest
  ): Promise<TradexModelsConfiguration.QuerySystemClientResponse> {
    const error: Errors.InvalidParameterError = new Errors.InvalidParameterError();
    Utils.validate(request.domain, 'domain')
      .setRequire()
      .throwValid(error);
    error.throwErr();
    const clientList: Client[] = await this.clientRepository.queryClientForSystemRequest(
      {
        domain: request.domain,
        lastQueriedTime:
          request.lastQueriedTime != null
            ? Utils.convertStringToDate(
                request.lastQueriedTime,
                Utils.DATETIME_DISPLAY_FORMAT
              )
            : null,
      }
    );
    const lastQueriedTime: string = Utils.formatDateToDisplay(
      new Date(),
      Utils.DATETIME_DISPLAY_FORMAT
    );
    return {
      clients: clientList.map(parseToClientResponse),
      lastQueriedTime: lastQueriedTime,
    };
  }

  public async findClientById(
    request: TradexModelsConfiguration.QueryClientByIdRequest
  ): Promise<TradexModelsConfiguration.QueryClientByIdResponse> {
    const invalidParams = new Errors.InvalidParameterError();
    validationUtils.validateId(request.id, invalidParams);
    invalidParams.throwErr();

    const client: Client = await this.clientRepository.findById(request);
    if (client == null) {
      throw new Errors.ObjectNotFoundError();
    }
    return parseToClientResponse(client);
  }

  public async addClient(
    request: TradexModelsConfiguration.PostClientRequest
  ): Promise<any> {
    const invalidParams = new Errors.InvalidParameterError();
    Utils.validate(request.userId, 'userId')
      .setRequire()
      .throwValid(invalidParams);
    Utils.validate(request.clientId, 'clientId')
      .setRequire()
      .throwValid(invalidParams);
    Utils.validate(request.clientSecret, 'clientSecret')
      .setRequire()
      .throwValid(invalidParams);
    Utils.validate(request.domain, 'domain')
      .setRequire()
      .throwValid(invalidParams);

    invalidParams.throwErr();

    const client = new Client();
    client.userId = request.userId;
    client.clientId = request.clientId;
    client.clientSecret = request.clientSecret;
    client.domain = request.domain;
    client.status = ClientStatusEnum.ENABLED;
    client.description = request.description != null ? request.description : '';

    if (request.loginMethodIds != null && request.loginMethodIds.length > 0) {
      const loginMethods: LoginMethod[] = await this.loginMethodRepository.findByIds(
        request.loginMethodIds
      );
      if (loginMethods.length < request.loginMethodIds.length) {
        throw new Errors.InvalidFieldValueError(
          'loginMethodIds',
          'LOGIN_METHOD_NOT_FOUND'
        );
      }
      client.loginMethods = loginMethods;
    }

    await this.clientRepository.save(client);

    Kafka.getInstance().sendMessage(
      uuid(),
      CLIENT_UPDATE_TOPIC,
      'newClient',
      request
    );
    if (config.domain === Utils.TRADEX_DOMAIN) {
      Kafka.getInstance().sendMessage(
        uuid(),
        CLIENT_SYNC_TOPIC,
        'newClient',
        request
      );
    }

    return;
  }

  public async updateClient(
    request: TradexModelsConfiguration.PutClientRequest
  ): Promise<TradexModelsConfiguration.PutClientResponse> {
    const invalidParams = new Errors.InvalidParameterError();
    validationUtils.validateId(request.id, invalidParams);
    invalidParams.throwErr();

    const client = await this.clientRepository.findOne(request.id);
    if (client == null) {
      throw new Errors.ObjectNotFoundError();
    }
    client.userId = request.userId != null ? request.userId : client.userId;
    client.clientId =
      request.clientId != null ? request.clientId : client.clientId;
    client.clientSecret =
      request.clientSecret != null ? request.clientSecret : client.clientSecret;
    client.description =
      request.description != null ? request.description : client.description;
    client.domain = request.domain != null ? request.domain : client.domain;

    if (request.loginMethodIds != null && request.loginMethodIds.length > 0) {
      const loginMethods: LoginMethod[] = await this.loginMethodRepository.findByIds(
        request.loginMethodIds
      );
      if (loginMethods.length < request.loginMethodIds.length) {
        throw new Errors.InvalidFieldValueError(
          'loginMethodIds',
          'LOGIN_METHOD_NOT_FOUND'
        );
      }
      client.loginMethods = loginMethods;
    }

    await this.clientRepository.save(client);

    Kafka.getInstance().sendMessage(
      uuid(),
      CLIENT_UPDATE_TOPIC,
      'updateClient',
      request
    );
    if (config.domain === Utils.TRADEX_DOMAIN) {
      Kafka.getInstance().sendMessage(
        uuid(),
        CLIENT_SYNC_TOPIC,
        'updateClient',
        request
      );
    }

    return {};
  }

  public async deleteClient(
    request: TradexModelsConfiguration.DeleteClientRequest
  ): Promise<TradexModelsConfiguration.DeleteClientResponse> {
    const invalidParams = new Errors.InvalidParameterError();
    validationUtils.validateId(request.id, invalidParams);
    invalidParams.throwErr();

    const client = await this.clientRepository.findOne(request.id);
    if (client == null) {
      throw new Errors.ObjectNotFoundError();
    }
    await this.clientRepository
      .createQueryBuilder()
      .update(Client)
      .set({ status: ClientStatusEnum.DISABLED })
      .where('id = :id', { id: request.id })
      .execute();

    Kafka.getInstance().sendMessage(
      uuid(),
      CLIENT_UPDATE_TOPIC,
      'deleteClient',
      request
    );
    if (config.domain === Utils.TRADEX_DOMAIN) {
      Kafka.getInstance().sendMessage(
        uuid(),
        CLIENT_SYNC_TOPIC,
        'deleteClient',
        request
      );
    }

    return {};
  }
}
