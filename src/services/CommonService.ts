import { Service as TypeService } from 'typedi';
import { Models } from 'tradex-common';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { ServiceRepository } from '../repositories/ServiceRepository';
import { parse, ServiceResponse } from '../models/response/ServiceResponse';
import Service from '../models/db/Service';

@TypeService()
export default class CommonService {
  @InjectRepository()
  private readonly serviceRepository: ServiceRepository;

  public async getAllServices(
    request: Models.IDataRequest
  ): Promise<ServiceResponse[]> {
    const response: Service[] = await this.serviceRepository.find();
    return response.map(parse);
  }
}
