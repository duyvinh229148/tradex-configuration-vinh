export const LOGIN_METHOD_UPDATE_TOPIC = 'client.loginMethod.update';

export const CLIENT_UPDATE_TOPIC = 'client.update';

export const SCOPE_UPDATE_TOPIC = 'scope.update';

export const SCOPE_GROUP_UPDATE_TOPIC = 'scope.scopeGroup.update';

//SYNC TOPIC
export const CLIENT_SYNC_TOPIC = 'client.sync';

export const SCOPE_SYNC_TOPIC = 'scope.sync';

export const SCOPE_GROUP_SYNC_TOPIC = 'scope.scopeGroup.sync';

export const LANG_KEY_SYNC_TOPIC = 'lang.key.sync';
