export enum DataViewStatusEnum {
  ENABLED = 'ENABLED',
  DISABLED = 'DISABLED',
}
