import config from './config';
import 'reflect-metadata';
import connection from './utils/dbConnection';
import { Connection } from 'typeorm';
import { AWS, Kafka, Logger, Utils } from 'tradex-common';
import { Container } from 'typedi';
import RequestHandler from './consumers/RequestHandler';
import ClientSyncHandler from './consumers/ClientSyncHandler';
import LangKeySyncHandler from './consumers/LangKeySyncHandler';
import ScopeSyncHandler from './consumers/ScopeSyncHandler';
import ScopeGroupSyncHandler from './consumers/ScopeGroupSyncHandler';

Logger.create(config.logger.config, true);
Logger.info('staring...');

connection
  .then(async (connection: Connection) => {
    Kafka.create(
      config,
      config.kafkaConsumerOptions,
      true,
      config.kafkaTopicOptions,
      config.kafkaProducerOptions
    );
    const requestHandler = Container.get(RequestHandler);
    requestHandler.init();

    AWS.loadCredentials(config.aws);

    if (config.domain !== Utils.TRADEX_DOMAIN) {
      const clientSyncHandler = Container.get(ClientSyncHandler);
      clientSyncHandler.init();
      const langKeySyncHandler = Container.get(LangKeySyncHandler);
      langKeySyncHandler.init();
      const scopeGroupSyncHandler = Container.get(ScopeGroupSyncHandler);
      scopeGroupSyncHandler.init();
      const scopeSyncHandler = Container.get(ScopeSyncHandler);
      scopeSyncHandler.init();
    }
  })
  .catch((error: any) => Logger.error(error));
