import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import Faq from '../models/db/Faq';

@Service()
@EntityRepository(Faq)
export class FaqRepository extends Repository<Faq> {
  public findById(id: number): Promise<Faq> {
    return this.findOne({ id: id });
  }
}
