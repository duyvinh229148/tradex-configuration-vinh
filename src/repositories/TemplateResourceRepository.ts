import { EntityRepository, Repository, In } from 'typeorm';
import { Service } from 'typedi';
import TemplateResource from '../models/db/TemplateResource';

@Service()
@EntityRepository(TemplateResource)
export class TemplateResourceRepository extends Repository<TemplateResource> {
  public findByMsName(msNames: string[]): Promise<TemplateResource[]> {
    return this.find({
      msName: In(msNames),
      isLatest: true,
    });
  }
}
