import { Service } from 'typedi';
import { EntityRepository, Repository } from 'typeorm';
import OpenApi from '../models/db/OpenApi';
import { DEFAULT_PAGE_SIZE } from '../constants';

@Service()
@EntityRepository(OpenApi)
export class OpenApiRepository extends Repository<OpenApi> {
  public findBy(
    filter: any,
    fetchCount: number = DEFAULT_PAGE_SIZE,
    sort: any = { id: 'ASC' }
  ): Promise<OpenApi[]> {
    return this.find({
      order: sort,
      where: filter,
      take: fetchCount,
    });
  }
}
