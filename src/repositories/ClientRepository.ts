import { Service } from 'typedi';
import Client from '../models/db/Client';
import { EntityRepository, Repository } from 'typeorm';
import config from '../config';
import IClientRequest from '../models/request/admin/IClientRequest';
import { ClientStatusEnum } from '../constants/ClientStatusEnum';
import { ISystemQuery } from '../models/request/admin/ISystemQueryRequest';
import LoginMethod from '../models/db/LoginMethod';
import { TradexModelsConfiguration } from 'tradex-models-ts';

@Service()
@EntityRepository(Client)
export class ClientRepository extends Repository<Client> {
  public async findByClientRequest(request: IClientRequest): Promise<Client[]> {
    let whereStr = ' client.domain = :domain AND client.status = :status ';
    const whereConditions: any = {
      domain: request.domain,
      status: ClientStatusEnum.ENABLED,
    };

    if (request.lastSequence != null) {
      whereStr += ' AND client.id > :lastSequence';
      whereConditions.lastSequence = request.lastSequence;
    }

    const response = this.createQueryBuilder('client')
      .take(
        request.fetchCount == null
          ? config.defaultFetchCount
          : request.fetchCount
      )
      .where(whereStr, whereConditions)
      .orderBy({
        'client.id': 'ASC',
      });
    if (request.isFullData === true) {
      return response
        .leftJoinAndSelect('client.loginMethods', 'loginMethod')
        .leftJoinAndSelect('loginMethod.scopeGroups', 'scopeGroup')
        .getMany();
    } else {
      return response.getMany();
    }
  }

  public async queryClientForSystemRequest(
    request: ISystemQuery
  ): Promise<Client[]> {
    let queryBuilder = this.createQueryBuilder('client')
      .leftJoinAndSelect('client.loginMethods', 'loginMethod')
      .leftJoinAndSelect('loginMethod.scopeGroups', 'scopeGroup')
      .where('client.domain = :domain', { domain: request.domain });
    if (request.lastQueriedTime != null) {
      queryBuilder.andWhere(
        'client.updated_at > :lastQueriedTime or loginMethod.updated_at > :lastQueriedTime',
        { lastQueriedTime: request.lastQueriedTime }
      );
    }
    queryBuilder = queryBuilder.orderBy({
      'client.id': 'ASC',
    });
    return queryBuilder.printSql().getMany();
  }

  public async findById(
    request: TradexModelsConfiguration.QueryClientByIdRequest
  ): Promise<Client> {
    return this.createQueryBuilder('client')
      .leftJoinAndSelect('client.loginMethods', 'loginMethod')
      .leftJoinAndSelect('loginMethod.scopeGroups', 'scopeGroup')
      .where('client.id = :id AND client.status = :status', {
        id: request.id,
        status: ClientStatusEnum.ENABLED,
      })
      .getOne();
  }

  public async queryClientIdMap(): Promise<Client[]> {
    const data = await this.createQueryBuilder('client')
      .leftJoinAndSelect('client.loginMethods', 'loginMethod')
      .getMany();
    data.map((obj: Client) => {
      obj.loginMethods = Object.assign(
        obj.loginMethods.map((value: LoginMethod) => value.id)
      );
    });
    return data;
  }
}
