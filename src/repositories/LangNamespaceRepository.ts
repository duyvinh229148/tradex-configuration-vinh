import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import LangNamespace from '../models/db/LangNamespace';

@Service()
@EntityRepository(LangNamespace)
export class LangNamespaceRepository extends Repository<LangNamespace> {
  public findById(namespaceId: number): Promise<LangNamespace> {
    return this.findOne({
      relations: [
        'langResource',
        'langResourceFiles',
        'langResource.langResourceVersions',
      ],
      where: {
        id: namespaceId,
      },
    });
  }
}
