import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import Menu from '../models/db/Menu';

@Service()
@EntityRepository(Menu)
export class MenuRepository extends Repository<Menu> {
  public findByRoleIds(menuRoleIds: number[]): Promise<Menu[]> {
    return this.createQueryBuilder('t1')
      .innerJoinAndSelect('t1.menuRoles', 't2')
      .innerJoinAndSelect('t1.menuGroup', 't3')
      .where('t2.id IN (:roleIds)', {
        roleIds: menuRoleIds,
      })
      .orderBy({
        't3.id': 'ASC',
        't1.order': 'ASC',
      })
      .getMany();
  }
}
