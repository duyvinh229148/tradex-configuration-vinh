import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import LangResourceFile from '../models/db/LangResourceFile';

@Service()
@EntityRepository(LangResourceFile)
export class LangResourceFileRepository extends Repository<LangResourceFile> {
  public findByMsName(msNames: string[]): Promise<LangResourceFile[]> {
    return this.createQueryBuilder('t1')
      .innerJoinAndSelect('t1.langNamespace', 't2')
      .innerJoinAndSelect('t2.langResource', 't3')
      .innerJoinAndSelect('t3.langResourceVersions', 't4')
      .where('t3.msName IN (:msNames)', { msNames: msNames })
      .orderBy({
        't3.msName': 'ASC',
        't1.lang': 'ASC',
      })
      .getMany();
  }
}
