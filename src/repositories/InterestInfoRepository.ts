import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import InterestInfo from '../models/db/InterestInfo';

@Service()
@EntityRepository(InterestInfo)
export class InterestInfoRepository extends Repository<InterestInfo> {}
