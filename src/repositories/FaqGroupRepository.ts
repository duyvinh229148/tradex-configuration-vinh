import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import FaqGroup from '../models/db/FaqGroup';

@Service()
@EntityRepository(FaqGroup)
export class FaqGroupRepository extends Repository<FaqGroup> {
  public findByMsNameAndLang(
    msName: string,
    lang: string
  ): Promise<FaqGroup[]> {
    return this.find({
      relations: ['faqs'],
      where: {
        msName: msName,
        lang: lang,
      },
    });
  }
}
