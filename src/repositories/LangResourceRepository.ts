import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import LangResource from '../models/db/LangResource';

@Service()
@EntityRepository(LangResource)
export class LangResourceRepository extends Repository<LangResource> {
  public getAllResources(): Promise<LangResource[]> {
    return this.find({
      relations: ['langNamespaces'],
    });
  }
}
