import { EntityRepository, Repository } from 'typeorm';
import { Service as TypeService } from 'typedi';
import Service from '../models/db/Service';

@TypeService()
@EntityRepository(Service)
export class ServiceRepository extends Repository<Service> {}
