import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import Holiday from '../models/db/Holiday';

@Service()
@EntityRepository(Holiday)
export class HolidayRepository extends Repository<Holiday> {}
