import config from '../config';
import { createConnection, useContainer } from 'typeorm';
import { Container } from 'typedi';

useContainer(Container);

const connection = createConnection({
  type: 'mysql',
  host: config.db.connection.host,
  port: config.db.connection.port,
  username: config.db.connection.user,
  password: config.db.connection.password,
  database: config.db.connection.database,
  entities: [`${__dirname}/../models/db/**/*.js`],
  synchronize: false,
  logging: true,
  timezone: 'Z',
});

export default connection;
