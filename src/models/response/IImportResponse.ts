export interface IImportResponse {
  status: number;
  statusText: string;
  importStatus: string;
  errMsg?: any;
}
