class TempCredentialResponse {
  public accessKeyId: string;
  public secretAccessKey: string;
  public sessionToken: string;
}

export { TempCredentialResponse };
