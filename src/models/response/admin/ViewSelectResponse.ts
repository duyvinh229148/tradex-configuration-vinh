class ViewSelectResponse {
  public id: any;
  public text: string;
  [key: string]: any;
}

export { ViewSelectResponse };
