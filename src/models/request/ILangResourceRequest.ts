import { Models } from 'tradex-common';

export default interface ILangResourceRequest extends Models.IDataRequest {
  msNames?: string[];
}
