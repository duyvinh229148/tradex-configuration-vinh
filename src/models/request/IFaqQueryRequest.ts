import { Models } from 'tradex-common';

export default interface IFaqQueryRequest extends Models.IDataRequest {
  msName: string;
}
