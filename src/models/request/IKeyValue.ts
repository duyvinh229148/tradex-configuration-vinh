export default interface IKeyValue {
  key: string;
  value: any;
}
